#!/usr/bin/env bash

sudo apt-get install -y mongodb-org
mkdir data
echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod
chmod a+x mongod

mv .env-example .env

nvm install 6.10
nvm alias default 6.10
