'use strict'

const pck = require('../package');
const cfg = require('../config');

const logger = require('bunyan').createLogger({name:`${pck.name}:${pck.version}`});
logger.level(cfg.logLevel);

module.exports = logger;